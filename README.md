# Price Tracker

As of January 2020, this project is no longer under development. The application does however work as it currently can track prices and store them locally. It even includes a graphical display.
However, it is missing many of the features we wished to implement.

Developed by 4 college students who aren't rich and wanted to experiment with automating something that could save assist in saving money.

The original intention of this application was to automatically purchase products for a user but it has since been switched over to only price tracking since purchase bots are explicitly illegal. 
This application will ask you to input some information about your product(such as name and website link).
It will then use Selenium to track for price changes off of a website based off of a time interval.
The information is stored locally, so you can access the data at any time.

Please be wary of accessing a website too many times or too quickly through this application as it may cause your IP to be blocked by a website.
We are not responsible if your IP is banned from a website as a result of using this application.